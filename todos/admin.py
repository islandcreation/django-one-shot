from django.contrib import admin
from todos.models import TodoList, TodoItem


# Feature 4 Admin Model
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]


# Feature 6 Register TodoItem model with admin
@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed",
    ]
